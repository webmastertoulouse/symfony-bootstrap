## Symfony Bootstrap

* Cette application fonctionne avec ODM doctrine pour persister les données via MongoDB
* Elle a un point API (/api/login) pour permettre à l'utilisateur de s'enregistrer via son email et mot de passe
* Elle a un point API (/api/erros) pour traiter et enregistrer les erreurs client (pratique quand on fonctionne avec un client Angular par exemple).
* EMail de confirmation envoyé aux nouveaux inscrits

### Quelques commandes en vrac

Traduction du Français :

`./bin/console translation:update --dump-messages --force fr`