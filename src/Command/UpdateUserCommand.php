<?php

namespace App\Command;

use App\Document\UserDocument;
use App\Manager\UserManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateUserCommand extends ContainerAwareCommand
{
    /**
     * @var UserManager $userService
     */
    private $userService;

    public function __construct(?string $name = null, UserManager $userService)
    {
        parent::__construct($name);

        $this->userService = $userService;
    }

    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('app:update_user')
            ->setDescription('Update a existing User')
            ->setHelp('This command allows you to create a user.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Mettre à jour un utilisateur',
            '============',
            '',
        ]);

        $user = $this->userService->getRepository()->findOneBy(['email' => 'jbespinasse@gmail.com']);
        if ($user) {
            $user->setUsername('jbespinasse');
            $user->setPassword('hercule');

            $this->userService->update($user);
        } else {
            $output->writeln([
                'Utilisateur non trouvé',
                '============',
                '',
            ]);
        }
    }
}