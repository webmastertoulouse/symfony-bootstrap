<?php

namespace App\Command;

use App\Document\UserDocument;
use App\Manager\UserManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateUserCommand extends ContainerAwareCommand
{
    /**
     * @var UserManager $userService
     */
    private $userService;

    public function __construct(?string $name = null, UserManager $userService)
    {
        parent::__construct($name);

        $this->userService = $userService;
    }

    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('app:create_user')
            ->setDescription('Creat a new User')
            ->setHelp('This command allows you to create a user.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Créer un utilisateur',
            '============',
            '',
        ]);

        $user = new UserDocument();
        $user->setUsername('jbespinasse');
        $user->setFirstname('JB');
        $user->setGivenname('Espinasse');
        $user->setEmail('jbespinasse@gmail.com');
        $user->setPassword('hercule');

        $this->userService->create($user);
    }
}