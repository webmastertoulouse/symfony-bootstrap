<?php

namespace App\Exception;

use Throwable;

class ODMValidatorException extends \Exception
{
    const VALIDATOR_EXCEPTION_USER_PRE_PERSISTS = 'orm.validator.exception.user.pre_persist';
    const VALIDATOR_EXCEPTION_USER_PRE_UPDATE   = 'orm.validator.exception.user.pre_update';
    const VALIDATOR_EXCEPTION_USER_EXISTS       = 'orm.validator.exception.user.exists';
    const VALIDATOR_EXCEPTION_EMPTY_VALUE       = 'orm.validator.exception.user.empty.value';

    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}