<?php

namespace App\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document(collection="log", repositoryClass="App\Repository\LogRepository")
 * @ODM\HasLifecycleCallbacks
 */
class LogDocument extends AbstractDocument
{
    /**
     * @var string
     * @ODM\Id(strategy="auto")
     */
    private $id;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $name;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $appId;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $time;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $location;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $url;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $status;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $message;

    /**
     * @var array
     * @ODM\Field(type="collection")
     */
    private $stack;

    /**
     * @var UserDocument
     * @ODM\ReferenceOne(targetDocument="UserDocument", inversedBy="Log", storeAs="id")
     */
    private $user;

    /**
     * @var date
     * @ODM\Field(name="created_at", type="date")
     * @ODM\AlsoLoad("created_at")
     */
    private $createdAt;

    /**
     * @var date
     * @ODM\Field(name="updated_at", type="date")
     * @ODM\AlsoLoad("updated_at")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->stack = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAppId(): string
    {
        return $this->appId;
    }

    /**
     * @param string $time
     */
    public function setTime(string $time): void
    {
        $this->time = $time;
    }

    /**
     * @return string
     */
    public function getTime(): string
    {
        return $this->time;
    }

    /**
     * @param string $appId
     */
    public function setAppId(string $appId): void
    {
        $this->appId = $appId;
    }

    /**
     * @return string
     */
    public function getLocation(): ?string
    {
        return $this->location;
    }

    /**
     * @param string $location
     */
    public function setLocation(?string $location): void
    {
        $this->location = $location;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return array
     */
    public function getStack(): ?array
    {
        return $this->stack;
    }

    /**
     * @param array $stack
     */
    public function setStack(?array $stack): void
    {
        $this->stack = $stack;
    }

    /**
     * @return UserDocument
     */
    public function getUser(): ?UserDocument
    {
        return $this->user;
    }

    /**
     * @param UserDocument $user
     */
    public function setUser(?UserDocument $user): void
    {
        $this->user = $user;
    }

    /**
     * @return date
     */
    public function getCreatedAt(): date
    {
        return $this->createdAt;
    }

    /**
     * @param date $createdAt
     */
    public function setCreatedAt(date $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return date
     */
    public function getUpdatedAt(): ?date
    {
        return $this->updatedAt;
    }

    /**
     * @param date $updatedAt
     */
    public function setUpdatedAt(?date $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

}