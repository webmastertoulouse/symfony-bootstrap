<?php

namespace App\Document\Filter;

use Doctrine\ODM\MongoDB\Mapping\ClassMetadata;
use Doctrine\ODM\MongoDB\Query\Filter\BsonFilter;

class DeletedAtFilter extends BsonFilter
{
    public function addFilterCriteria(ClassMetadata $class)
    {
        if (!$class->reflClass->hasProperty('deletedAt')) {
            return array();
        }

        return ['deletedAt' => null];
    }
}