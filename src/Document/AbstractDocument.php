<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

class AbstractDocument
{
    /**
     * @param LifecycleEventArgs $eventArgs
     * @ODM\PrePersist
     */
    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        $document = $eventArgs->getDocument();
        $document->setCreatedAt(new Datetime());
    }

    /**
     * @ODM\PreUpdate
     */
    public function preUpdate(LifecycleEventArgs $eventArgs)
    {
        $document = $eventArgs->getDocument();
        $document->setUpdatedAt(new Datetime());

        $dm = $eventArgs->getDocumentManager();
        $class = $dm->getClassMetadata(get_class($document));
        $dm->getUnitOfWork()->recomputeSingleDocumentChangeSet($class, $document);
    }
}