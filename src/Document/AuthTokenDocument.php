<?php

namespace App\Document;

use App\Exception\ODMValidatorException;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use Datetime;

/**
 * @ODM\Document(collection="auth_token", repositoryClass="App\Repository\AuthTokenRepository")
 * @ODM\HasLifecycleCallbacks
 */
class AuthTokenDocument implements DocumentInterface
{
    /**
     * @var string
     * @ODM\Id(strategy="auto")
     */
    private $id;

    /**
     * @var string
     * @ODM\Field(type="string")
     * @ODM\UniqueIndex
     */
    private $value;

    /**
     * @ODM\ReferenceOne(targetDocument="UserDocument", inversedBy="authToken")
     * @ODM\UniqueIndex
     */
    private $user;

    /**
     * @var date
     * @ODM\Field(name="created_at", type="date")
     * @ODM\AlsoLoad("created_at")
     */
    private $createdAt;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt(): ?Datetime
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @ODM\PrePersist
     */
    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        /** @var UserDocument $document */
        $document = $eventArgs->getDocument();
        $document->setCreatedAt(new Datetime());

        if (!$document->getValue()) {
            throw new ODMValidatorException(ODMValidatorException::VALIDATOR_EXCEPTION_EMPTY_VALUE);
        }
    }

    /**
     * @ODM\PreUpdate
     * @param LifecycleEventArgs $eventArgs
     */
    public function preUpdate(\Doctrine\Common\Persistence\Event\LifecycleEventArgs $eventArgs)
    {
        /** @var UserDocument $document */
        $document = $eventArgs->getDocument();
        $document->setCreatedAt(new Datetime());

        if (!$document->getValue()) {
            throw new ODMValidatorException(ODMValidatorException::VALIDATOR_EXCEPTION_EMPTY_VALUE);
        }

        $dm = $eventArgs->getDocumentManager();
        $class = $dm->getClassMetadata(get_class($document));
        $dm->getUnitOfWork()->recomputeSingleDocumentChangeSet($class, $document);
    }
}