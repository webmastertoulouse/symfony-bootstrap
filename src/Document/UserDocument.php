<?php

namespace App\Document;

use App\Exception\ODMValidatorException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use Datetime;

/**
 * @ODM\Document(collection="user", repositoryClass="App\Repository\UserRepository")
 * @ODM\UniqueIndex(keys={"email"="asc", "username"="asc"})
 * @ODM\HasLifecycleCallbacks
 */
class UserDocument extends AbstractDocument implements UserInterface, DocumentInterface
{
    /**
     * @var string
     * @ODM\Id(strategy="auto")
     */
    private $id;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $username;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $email;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $firstname;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $givenname;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $password;

    /**
     * @var string
     */
    private $plainPassword;

    /**
     * @var boolean
     * @ODM\Field(type="boolean")
     */
    private $enabled;

    /**
     * @var boolean
     * @ODM\Field(type="boolean")
     */
    private $confirmed;

    /**
     * @var string
     * @ODM\Field(type="string")
     * @ODM\AlsoLoad("confirm_key")
     */
    private $confirmKey;

    /**
     * @var array
     * @ODM\Field(type="collection")
     */
    private $roles;

    /**
     * @ODM\ReferenceOne(targetDocument="AuthTokenDocument", mappedBy="user", orphanRemoval=true)
     * @ODM\AlsoLoad("auth_token")
     * @ODM\DiscriminatorField("type")
     */
    private $authToken;

    /**
     * @ODM\ReferenceMany(targetDocument="LogDocument", mappedBy="user", orphanRemoval=true)
     * @ODM\AlsoLoad("log")
     * @ODM\DiscriminatorField("type")
     */
    private $log;

    /**
     * @var date
     * @ODM\Field(name="deleted_at", type="date")
     * @ODM\AlsoLoad("deleted_at")
     */
    private $deletedAt;

    /**
     * @var date
     * @ODM\Field(name="created_at", type="date")
     * @ODM\AlsoLoad("created_at")
     */
    private $createdAt;

    /**
     * @var date
     * @ODM\Field(name="updated_at", type="date")
     * @ODM\AlsoLoad("updated_at")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->roles    = [];
        $this->log      = new ArrayCollection();

        $this->addRoles('ROLE_USER');
        $this->setEnabled(true);
        $this->setConfirmed(false);
    }

    /**
     * @return mixed
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getGivenname(): ?string
    {
        return $this->givenname;
    }

    /**
     * @param mixed $givenname
     */
    public function setGivenname($givenname): void
    {
        $this->givenname = $givenname;
    }

    /**
     * @return mixed
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword($plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }

    /**
     * @return bool
     */
    public function isConfirmed(): bool
    {
        return $this->confirmed;
    }

    /**
     * @param bool $confirmed
     */
    public function setConfirmed(bool $confirmed): void
    {
        $this->confirmed = $confirmed;
    }

    /**
     * @param null|string $confirmKey
     */
    public function setConfirmKey(?string $confirmKey): void
    {
        $this->confirmKey = $confirmKey;
    }

    /**
     * @return null|string
     */
    public function getConfirmKey(): ?string
    {
        return $this->confirmKey;
    }

    /**
     * @return mixed
     */
    public function getRoles(): ?array
    {
        return $this->roles;
    }

    /**
     * @param mixed $roles
     */
    public function setRoles($roles): void
    {
        $this->roles = $roles;
    }

    /**
     * @param $role
     */
    public function addRoles($role): void
    {
        array_push($this->roles, $role);
    }

    /**
     * @return string
     */
    public function getAuthToken()
    {
        return $this->authToken;
    }

    /**
     * @param $authToken
     */
    public function setAuthToken($authToken)
    {
        $this->authToken = $authToken;
    }

    /**
     * @return LogDocument[]
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * @param $log
     */
    public function setLog($log)
    {
        $this->log = $log;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt(): ?Datetime
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt(): ?Datetime
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt(): ?Datetime
    {
        return $this->deletedAt;
    }

    /**
     * @param mixed $deletedAt
     */
    public function setDeletedAt($deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @ODM\PrePersist
     */
    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        /** @var UserDocument $document */
        $document = $eventArgs->getDocument();
        $document->setCreatedAt(new Datetime());

        if (false == $this->validateEmail($document->getEmail())) {
            throw new ODMValidatorException(ODMValidatorException::VALIDATOR_EXCEPTION_USER_PRE_PERSISTS);
        }

        if (!$document->getUsername()) {
            $document->setUsername(substr($document->getEmail(), 0, strpos($document->getEmail(), '@')));
        }

        if (!$document->getPassword()) {
            throw new ODMValidatorException(ODMValidatorException::VALIDATOR_EXCEPTION_EMPTY_VALUE);
        }
    }

    /**
     * @ODM\PreUpdate
     */
    public function preUpdate(LifecycleEventArgs $eventArgs)
    {
        /** @var UserDocument $document */
        $document = $eventArgs->getDocument();
        $document->setUpdatedAt(new Datetime());

        if (false == $this->validateEmail($document->getEmail())) {
            throw new ODMValidatorException(ODMValidatorException::VALIDATOR_EXCEPTION_USER_PRE_UPDATE);
        }

        if (!$document->getUsername()) {
            throw new ODMValidatorException(ODMValidatorException::VALIDATOR_EXCEPTION_EMPTY_VALUE);
        }

        $dm = $eventArgs->getDocumentManager();
        $class = $dm->getClassMetadata(get_class($document));
        $dm->getUnitOfWork()->recomputeSingleDocumentChangeSet($class, $document);
    }

    /**
     * @param $email
     * @return boolean
     */
    private function validateEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
}