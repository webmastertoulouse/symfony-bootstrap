<?php

namespace App\Request\ParamConverter;

use Doctrine\ODM\MongoDB\DocumentManager;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class JsonConverter implements ParamConverterInterface
{
    /**
     * @var DocumentManager
     */
    private $documentManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param DocumentManager $documentManager
     * @param LoggerInterface $logger
     *
     */
    public function __construct(DocumentManager $documentManager, LoggerInterface $logger)
    {
        $this->documentManager  = $documentManager;
        $this->logger           = $logger;
    }

    /**
     * @param Request $request
     * @param ParamConverter $configuration
     * @return bool|void
     *
     * @deprecated use DTOResolver instead of
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $class      = $configuration->getClass();
        $entity     = new $class();
        $entity->setRequest($request);
        $data       = json_decode($request->getContent(), true);
        $id         = isset($data['id']) ?: null;

        if (!$data) {
            $request->attributes->set($configuration->getName(), null);
            return;
        }

        if ($id) {
            $originalEntity = $this->documentManager->find($class, $id);
            $request->attributes->set('original' . ucfirst($configuration->getName()), $originalEntity);
        }

        foreach ($data AS $key=>$val) {
            if (property_exists($class, $key) && method_exists($class, 'set' . $key)) {
                $entity->{'set' . ucfirst($key)}($val);
            }
        }

        $request->attributes->set($configuration->getName().'Data', $data);
        $request->attributes->set($configuration->getName(), $entity);
    }

    public function supports(ParamConverter $configuration)
    {
        if ($configuration->getClass() && class_exists($configuration->getClass())) {
            return true;
        }

        return false;
    }
}