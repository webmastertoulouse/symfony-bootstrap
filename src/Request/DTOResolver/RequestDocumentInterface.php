<?php

namespace App\Request\DTOResolver;

use App\Document\DocumentInterface;

interface RequestDocumentInterface
{
    public function toDTO(DocumentInterface $document);
    public function fromDTO();
}