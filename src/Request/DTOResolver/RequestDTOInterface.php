<?php

namespace App\Request\DTOResolver;

use App\Document\DocumentInterface;
use Symfony\Component\HttpFoundation\Request;

interface RequestDTOInterface
{
    public function setDocument(DocumentInterface $document);
    public function setRequest(Request $request);
    public function toArray();
}