<?php

namespace App\Request\DTO;

use App\Document\DocumentInterface;
use App\Document\UserDocument;
use App\Request\DTOResolver\RequestDocumentInterface;
use App\Request\DTOResolver\RequestDTOInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;


use Symfony\Component\HttpFoundation\Request;

class UserDTO implements RequestDTOInterface, RequestDocumentInterface
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $givenname;

    /**
     * @var string
     */
    private $password;

    /**
     * @var AuthTokenDTO
     */
    private $authToken;

    /**
     * @var boolean
     */
    private $enabled;

    /**
     * @var boolean
     */
    private $confirmed;

    /**
     * @var string
     */
    private $confirmKey;

    /**
     * @var array
     */
    private $roles;

    /**
     * @var date
     */
    private $createdAt;

    /**
     * @var date
     */
    private $updatedAt;

    public function __construct()
    {
        $this->roles = [];
    }

    public function setDocument(DocumentInterface $document)
    {
        $this->setId($document->getId());
        $this->setUsername($document->getUsername());
        $this->setEmail($document->getEmail());
        $this->setFirstname($document->getFirstname());
        $this->setGivenname($document->getGivenname());
        $this->setEnabled($document->isEnabled());
        $this->setRoles($document->getRoles());
    }

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(?string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname(?string $firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getGivenname(): ?string
    {
        return $this->givenname;
    }

    /**
     * @param string $givenname
     */
    public function setGivenname(?string $givenname): void
    {
        $this->givenname = $givenname;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(?string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return bool
     */
    public function isEnabled(): ?bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(?bool $enabled): void
    {
        $this->enabled = $enabled;
    }

    /**
     * @return bool
     */
    public function isConfirmed(): ?bool
    {
        return $this->confirmed;
    }

    /**
     * @param bool $confirmed
     */
    public function setConfirmed(?bool $confirmed): void
    {
        $this->confirmed = $confirmed;
    }

    /**
     * @param null|string $confirmKey
     */
    public function setConfirmKey(?string $confirmKey): void
    {
        $this->confirmKey = $confirmKey;
    }

    /**
     * @return null|string
     */
    public function getConfirmKey(): ?string
    {
        return $this->confirmKey;
    }

    /**
     * @return AuthTokenDTO
     */
    public function getAuthToken()
    {
        return $this->authToken;
    }

    /**
     * @param AuthTokenDTO $authToken
     */
    public function setAuthToken(AuthTokenDTO $authToken): void
    {
        $this->authToken = $authToken;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     */
    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    /**
     * @param $role
     */
    public function addRoles($role): void
    {
        array_push($this->roles, $role);
    }

    /**
     * @return string
     */
    public function getCreatedAt(): ?string
    {
        return $this->createdAt ? $this->createdAt->format('Y-m-d') : null;
    }

    /**
     * @param Datetime $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt ? $this->updatedAt->format('Y-m-d') : null;
    }

    /**
     * @param date $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->setId($request->get('id'));
        $this->setUsername($request->get('username'));
        $this->setEmail($request->get('email'));
        $this->setFirstname($request->get('firstname'));
        $this->setGivenname($request->get('givenname'));
        $this->setPassword($request->get('password'));
        if (is_bool($request->get('enabled'))) {
            $this->setEnabled($request->get('enabled'));
        }
        if (is_array($request->get('roles'))) {
            foreach ($request->get('roles') as $role) {
                $this->addRoles($role);
            }
        }
    }

    /**
     * @param DocumentInterface $document
     * @return UserDTO
     */
    public function toDTO(DocumentInterface $document)
    {
        $dto = new UserDTO();
        $dto->setId($document->getId());
        $dto->setUsername($document->getUsername());
        $dto->setEmail($document->getEmail());
        $dto->setFirstname($document->getFirstname());
        $dto->setGivenname($document->getGivenname());
        // $dto->setPassword($document->getPassword());
        /* if (is_bool($document->isEnabled())) {
            $dto->setEnabled($document->isEnabled());
        } */
        if (is_bool($document->isConfirmed())) {
            $dto->setConfirmed($document->isConfirmed());
        }
        /* if (is_array($document->getRoles())) {
            foreach ($document->getRoles() as $role) {
                $dto->addRoles($role);
            };
        } */
        $dto->setCreatedAt($document->getCreatedAt());
        // $dto->setUpdatedAt($document->getUpdatedAt());

        return $dto;
    }

    /**
     * @return UserDocument
     */
    public function fromDTO()
    {
        $document = new UserDocument();
        $document->setId($this->getId());
        $document->setUsername($this->getUsername());
        $document->setEmail($this->getEmail());
        $document->setFirstname($this->getFirstname());
        $document->setGivenname($this->getGivenname());
        $document->setPassword($this->getPassword());
        if (is_bool($this->isEnabled())) {
            $document->setEnabled($this->isEnabled());
        }
        if (is_bool($this->isConfirmed())) {
            $document->setConfirmed($this->isConfirmed());
        }
        if (is_array($this->getRoles())) {
            foreach ($this->getRoles() as $role) {
                $document->addRoles($role);
            }
        }
        $document->setUpdatedAt($this->getUpdatedAt());
        $document->setCreatedAt($this->getCreatedAt());

        return $document;
    }

    public function toArray()
    {
        $userR = [];
        $userR['id']            = $this->getId();
        $userR['username']      = $this->getUsername();
        $userR['email']         = $this->getEmail();
        $userR['firstname']     = $this->getFirstname();
        $userR['givenname']     = $this->getGivenname();
        $userR['password']      = $this->getPassword();
        $userR['enabled']       = $this->isEnabled();
        $userR['confirmed']     = $this->isConfirmed();
        $userR['roles']         = $this->getRoles();
        $userR['updated_at']    = $this->getUpdatedAt();
        $userR['created_at']    = $this->getCreatedAt();

        return $userR;
    }
}