<?php

namespace App\Request\DTO;

use App\Document\DocumentInterface;
use App\Document\LogDocument;
use App\Request\DTOResolver\RequestDocumentInterface;
use App\Request\DTOResolver\RequestDTOInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class LogDTO implements RequestDTOInterface, RequestDocumentInterface
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $appId;

    /**
     * @var string
     */
    private $time;

    /**
     * @var string
     */
    private $location;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $message;

    /**
     * @var array
     */
    private $stack;

    /**
     * @var array
     */
    private $user;

    /**
     * @var date
     */
    private $createdAt;

    /**
     * @var date
     */
    private $updatedAt;

    public function __construct()
    {
        $this->stack = [];
    }

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAppId(): string
    {
        return $this->appId;
    }

    /**
     * @param string $appId
     */
    public function setAppId(string $appId): void
    {
        $this->appId = $appId;
    }

    /**
     * @param string $time
     */
    public function setTime(string $time): void
    {
        $this->time = $time;
    }

    /**
     * @return string
     */
    public function getTime(): string
    {
        return $this->time;
    }

    /**
     * @return string
     */
    public function getLocation(): ?string
    {
        return $this->location;
    }

    /**
     * @param string $location
     */
    public function setLocation(?string $location): void
    {
        $this->location = $location;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return array
     */
    public function getStack(): ?array
    {
        return $this->stack;
    }

    /**
     * @param array $stack
     */
    public function setStack(?array $stack): void
    {
        $this->stack = $stack;
    }

    /**
     * @return array
     */
    public function getUser(): ?array
    {
        return $this->user;
    }

    /**
     * @param array $user
     */
    public function setUser(?array $user): void
    {
        $this->user = $user;
    }

    /**
     * @return date
     */
    public function getCreatedAt(): date
    {
        return $this->createdAt;
    }

    /**
     * @param date $createdAt
     */
    public function setCreatedAt(date $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return date
     */
    public function getUpdatedAt(): ?date
    {
        return $this->updatedAt;
    }

    /**
     * @param date $updatedAt
     */
    public function setUpdatedAt(?date $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @param DocumentInterface $document
     */
    public function setDocument(DocumentInterface $document)
    {
        $this->setId($document->getId());
        $this->setName($document->getName());
        $this->setAppId($document->getAppId());
        $this->setTime($document->getTime());
        $this->setLocation($document->getLocation());
        $this->setUrl($document->getUrl());
        $this->setStatus($document->getStatus());
        $this->setMessage($document->getMessage());
        $this->setStack($document->getStack());
        $this->setUser($document->getUser());
    }

    /**
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->setId($request->get('id'));
        $this->setName($request->get('name'));
        $this->setAppId($request->get('appId'));
        $this->setTime($request->get('time'));
        $this->setLocation($request->get('location'));
        $this->setUrl($request->get('url'));
        $this->setStatus($request->get('status'));
        $this->setMessage($request->get('message'));
        $this->setStack($request->get('stack'));
    }

    /**
     * @param DocumentInterface $document
     * @return LogDTO
     */
    public function toDTO(DocumentInterface $document)
    {
        $dto = new LogDTO();
        $dto->setId($document->getId());
        $dto->setName($document->getName());
        $dto->setAppId($document->getAppId());
        $dto->setTime($document->getTime());
        $dto->setLocation($document->getLocation());
        $dto->setUrl($document->getUrl());
        $dto->setStatus($document->getStatus());
        $dto->setMessage($document->getMessage());
        $dto->setStack($document->getStack());
        $dto->setUser($document->getUser());

        return $dto;
    }

    /**
     * @return LogDocument
     */
    public function fromDTO()
    {
        $document = new LogDocument();
        $document->setId($this->getId());
        $document->setName($this->getName());
        $document->setAppId($this->getAppId());
        $document->setTime($this->getTime());
        $document->setLocation($this->getLocation());
        $document->setUrl($this->getUrl());
        $document->setStatus($this->getStatus());
        $document->setMessage($this->getMessage());
        $document->setStack($this->getStack());

        return $document;
    }

    public function toArray() {}
}