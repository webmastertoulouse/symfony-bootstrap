<?php

namespace App\Request\DTO;

use App\Document\AuthTokenDocument;
use App\Document\DocumentInterface;
use App\Request\DTOResolver\RequestDocumentInterface;
use App\Request\DTOResolver\RequestDTOInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class AuthTokenDTO implements RequestDTOInterface, RequestDocumentInterface
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $value;

    /**
     * @var UserDTO
     */
    private $user;

    /**
     * @var date
     * @Assert\NotBlank()
     */
    private $createdAt;

    public function __construct()
    {
    }

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt(): ?\Datetime
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @param DocumentInterface $document
     */
    public function setDocument(DocumentInterface $document)
    {
        $this->setValue($document->getValue());
        $this->setCreatedAt($document->getCreatedAt());
    }

    /**
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->setValue($request->get('value'));
        $this->setCreatedAt($request->get('createdAt'));
    }

    /**
     * @param DocumentInterface $document
     * @return AuthTokenDTO
     */
    public function toDTO(DocumentInterface $document)
    {
        $dto = new AuthTokenDTO();
        $dto->setValue($document->getValue());
        $dto->setCreatedAt($document->getCreatedAt());

        return $dto;
    }

    /**
     * @return AuthTokenDocument
     */
    public function fromDTO()
    {
        $document = new AuthTokenDocument();
        $document->setValue($this->getValue());
        $document->setCreatedAt($this->getCreatedAt());

        return $document;
    }

    public function toArray() {}
}