<?php

namespace App\Request\DTO;

use App\Document\DocumentInterface;
use App\Request\DTOResolver\RequestDTOInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class LoginDTO implements RequestDTOInterface
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $login;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $password;

    public function __construct()
    {
    }

    /**
     * @return string
     */
    public function getLogin(): ?string
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin($login): void
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->setLogin($request->get('login'));
        $this->setPassword($request->get('password'));
    }

    public function setDocument(DocumentInterface $document)
    {
        // TODO: Implement setDocument() method.
    }

    public function toArray() {}
}