<?php

namespace App\Service;

use Symfony\Component\Debug\Exception\ClassNotFoundException;
use Symfony\Component\Serializer\SerializerInterface;

class SerializerService
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param $class
     * @param $object
     * @param string $format
     * @return string
     * @throws ClassNotFoundException
     */
    public function serialize($class, $object, $format = 'json')
    {
        if (!class_exists($class)) {
            throw new ClassNotFoundException();
        }

        $dto        = new $class();
        $dtoObject  = $dto->toDTO($object);

        return $this->serializer->serialize($dtoObject, $format);
    }
}