<?php

namespace App\Security\User;

use App\Document\AuthTokenDocument;
use App\Document\UserDocument;
use App\Manager\AuthTokenManager;
use App\Manager\UserManager;
use Documents\User;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class AuthTokenUserProvider implements UserProviderInterface
{
    /** @var AuthTokenManager */
    private $authTokenManager;

    /** @var UserManager */
    private $userManager;

    /**
     * @param UserManager $userManager
     * @param AuthTokenManager $authTokenManager
     */
    public function __construct(UserManager $userManager, AuthTokenManager $authTokenManager)
    {
        $this->userManager      = $userManager;
        $this->authTokenManager = $authTokenManager;
    }

    /**
     * @param $authTokenHeader
     * @return null|AuthTokenDocument
     */
    public function getAuthToken($authTokenHeader)
    {
        return $this->authTokenManager->getRepository()->findOneBy([
            'value' => $authTokenHeader
        ]);
    }

    public function loadUserByUsername($email)
    {
        /** @var UserDocument $user */
        $user = $this->userManager->getRepository()->findOneBy([
            'email' => $email
        ]);

        if ($user && $user->isEnabled()) {
            return new AuthTokenUser(
                $email,
                $user->getPassword(),
                $user->getSalt(),
                $user->getRoles()
            );
        }

        throw new UsernameNotFoundException(
            sprintf('Email "%s" does not exist.', $email)
        );
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof AuthTokenUser) {
            throw new UnsupportedUserException(
                sprintf('Instance of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return AuthTokenUser::class === $class;
    }
}