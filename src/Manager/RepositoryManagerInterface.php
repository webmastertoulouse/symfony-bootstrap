<?php

namespace App\Manager;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\DocumentRepository;

interface RepositoryManagerInterface
{
    /**
     * @return DocumentRepository
     */
    public function getRepository();

    /**
     * @return DocumentManager
     */
    public function getDocumentManager();

    /**
     * @param DocumentManager $documentManager
     */
    public function setDocumentManager(DocumentManager $documentManager);
}