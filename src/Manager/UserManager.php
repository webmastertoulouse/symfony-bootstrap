<?php

namespace App\Manager;

use App\Document\DocumentInterface;
use App\Document\UserDocument;
use App\Exception\ODMValidatorException;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\DocumentRepository;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Translation\TranslatorInterface;

class UserManager implements RepositoryManagerInterface, ODMManagerInterface
{
    /**
     * @var LoggerInterface $logger
     */
    private $logger;

    /**
     * @var DocumentManager $documentManager
     */
    private $documentManager;

    /**
     * @var UserPasswordEncoderInterface $encoder
     */
    private $encoder;

    /**
     * @var \Swift_Mailer
     */
    private $swiftMailer;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var \Twig_Environment
     */
    private $environment;

    /**
     * @var array
     */
    private $mailerParameters;

    /**
     * @param LoggerInterface $logger
     * @param DocumentManager $documentManager
     * @param UserPasswordEncoderInterface $encoder
     * @param \Swift_Mailer $swift_Mailer
     * @param TranslatorInterface $translator
     * @param \Twig_Environment $environment
     * @param array $mailerParameters
     */
    public function __construct(LoggerInterface $logger,
                                DocumentManager $documentManager,
                                UserPasswordEncoderInterface $encoder,
                                \Swift_Mailer $swift_Mailer,
                                TranslatorInterface $translator,
                                \Twig_Environment $environment,
                                array $mailerParameters)
    {
        $this->logger               = $logger;
        $this->documentManager      = $documentManager;
        $this->encoder              = $encoder;
        $this->swiftMailer          = $swift_Mailer;
        $this->translator           = $translator;
        $this->environment          = $environment;
        $this->mailerParameters     = $mailerParameters;
    }

    /**
     * @param UserDocument $user
     * @param string $plainPassword
     * @return string
     */
    public function encodePassword(UserDocument $user, $plainPassword)
    {
        return $this->encoder->encodePassword($user, $plainPassword);
    }

    /**
     * @param UserDocument $user
     * @param $password
     * @return bool
     */
    public function isPasswordValid(UserDocument $user, $password)
    {
        return $this->encoder->isPasswordValid($user, $password);
    }

    /**
     * @param UserDocument $user
     * @return UserDocument
     */
    public function create(DocumentInterface $user)
    {
        if ($user->getPlainPassword()) {
            $password = $this->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
        } else if ($user->getPassword()) {
            $password = $this->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
        }

        $user->setConfirmKey($this->generateConfirmKey());

        $this->documentManager->persist($user);
        $this->documentManager->flush();

        $this->_sendConfirmMail($user);

        return $user;
    }

    /**
     * @param UserDocument $user
     * @param UserDocument $originalUser
     */
    public function update(DocumentInterface $user, DocumentInterface $originalUser)
    {
        if ($user->getPlainPassword()) {
            $password = $this->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
        }

        if ($user->getUsername()) {
            $originalUser->setUsername($user->getUsername());
        }

        if ($user->getEmail()) {
            $originalUser->setEmail($user->getEmail());
        }

        if ($user->getFirstname()) {
            $originalUser->setFirstname($user->getFirstname());
        }

        if ($user->getGivenname()) {
            $originalUser->setGivenname($user->getGivenname());
        }

        $this->documentManager->persist($originalUser);
        $this->documentManager->flush();

        return $originalUser;
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository|DocumentRepository
     */
    public function getRepository()
    {
        return $this->documentManager->getRepository('App\Document\UserDocument');
    }

    /**
     * @return DocumentManager
     */
    public function getDocumentManager()
    {
        return $this->documentManager;
    }

    /**
     * @param DocumentManager $documentManager
     */
    public function setDocumentManager(DocumentManager $documentManager)
    {
        $this->documentManager = $documentManager;
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function generateConfirmKey()
    {
        return base64_encode(random_bytes(200));
    }

    /**
     * @param UserDocument $user
     */
    private function _sendConfirmMail(UserDocument $user)
    {
        if (!$user->getConfirmKey()) {
            $user->setConfirmKey($this->generateConfirmKey());
            $this->documentManager->persist($user);
            $this->documentManager->flush();
        }

        $message = (new \Swift_Message($this->translator->trans('confirm_account.subject')))
            ->setFrom($this->mailerParameters['mail_from'])
            ->setTo($user->getEmail())
            ->setBody(
                $this->environment->render(
                    'emails/user/confirm-account.html.twig',
                    [
                        'confirmKey'    => $user->getConfirmKey()
                    ]
                ),
                'text/html'
            )
            ->addPart(
                $this->environment->render(
                    'emails/user/confirm-account.txt.twig',
                    [
                        'confirmKey'    => $user->getConfirmKey()
                    ]
                ),
                'text/plain'
            )
        ;

        $this->swiftMailer->send($message);

        return $user;
    }
}