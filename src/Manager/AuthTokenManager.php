<?php

namespace App\Manager;

use App\Document\AuthTokenDocument;
use App\Document\DocumentInterface;
use App\Document\UserDocument;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\DocumentRepository;
use Psr\Log\LoggerInterface;

class AuthTokenManager implements RepositoryManagerInterface
{
    /**
     * @var LoggerInterface $logger
     */
    private $logger;

    /**
     * @var DocumentManager $documentManager
     */
    private $documentManager;

    /**
     * @param LoggerInterface $logger
     * @param DocumentManager $documentManager
     */
    public function __construct(LoggerInterface $logger, DocumentManager $documentManager)
    {
        $this->logger               = $logger;
        $this->documentManager      = $documentManager;
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository|DocumentRepository
     */
    public function getRepository()
    {
        return $this->documentManager->getRepository('App\Document\AuthTokenDocument');
    }

    /**
     * @return DocumentManager
     */
    public function getDocumentManager()
    {
        return $this->documentManager;
    }

    /**
     * @param DocumentManager $documentManager
     */
    public function setDocumentManager(DocumentManager $documentManager)
    {
        $this->documentManager = $documentManager;
    }

    /**
     * @param UserDocument $user
     * @return AuthTokenDocument
     */
    public function getAuthToken(UserDocument $user)
    {
        $authToken = $user->getAuthToken();

        $base64Encode = base64_encode(random_bytes(50));
        if ($authToken) {
            $authToken->setValue($base64Encode);
        } else {
            $authToken = new AuthTokenDocument();
            $authToken->setValue($base64Encode);
            $authToken->setUser($user);
        }

        $this->getDocumentManager()->persist($authToken);
        $this->getDocumentManager()->flush();

        return $authToken;
    }
}