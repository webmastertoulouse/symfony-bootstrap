<?php

namespace App\Manager;

use App\Document\DocumentInterface;

interface ODMManagerInterface
{
    public function create(DocumentInterface $document);
    public function update(DocumentInterface $document, DocumentInterface $originalDocument);
}