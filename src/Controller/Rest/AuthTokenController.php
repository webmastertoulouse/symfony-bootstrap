<?php

namespace App\Controller\Rest;

use App\Document\AuthTokenDocument;
use App\Request\DTO\LoginDTO;
use App\Document\UserDocument;
use App\Manager\AuthTokenManager;
use App\Manager\UserManager;
use App\Service\SerializerService;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Serializer\SerializerInterface;

class AuthTokenController extends FOSRestController
{
    /**
     * @var AuthTokenManager
     */
    private $authTokenManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SerializerInterface
     */
    private $serializerService;

    /**
     * @param LoggerInterface $logger
     * @param UserManager $userManager
     * @param AuthTokenManager $authTokenManager
     * @param SerializerService $serializerService
     */
    public function __construct(LoggerInterface $logger, UserManager $userManager, AuthTokenManager $authTokenManager, SerializerService $serializerService)
    {
        $this->logger           = $logger;
        $this->authTokenManager = $authTokenManager;
        $this->userManager      = $userManager;
        $this->serializerService= $serializerService;
    }

    /**
     * @Route("/login", methods={"POST"})
     *
     * @param LoginDTO $loginDTO
     * @return View
     *
     * @throws BadCredentialsException
     */
    public function postAuthTokenAction(LoginDTO $loginDTO)
    {
        /** @var UserDocument $user */
        $user = $this->userManager->getRepository()->findOneBy([
            'email' => $loginDTO->getLogin()
        ]);

        if (!$user || !$user->isEnabled()) {
            throw new BadCredentialsException();
        }

        $isPasswordValid = $this->userManager->isPasswordValid($user, $loginDTO->getPassword());

        if (!$isPasswordValid) {
            throw new BadCredentialsException();
        }

        $authToken = $this->authTokenManager->getAuthToken($user);

        $userContent        = $this->serializerService->serialize('App\Request\DTO\UserDTO', $user, 'json');
        $authTokenContent   = $this->serializerService->serialize('App\Request\DTO\AuthTokenDTO', $authToken, 'json');

        return View::create([
            'success'       => true,
            'auth_token'    => $authTokenContent,
            'user'          => $userContent
        ], Response::HTTP_OK);
    }

    /**
     * @Route("/logout", methods={"POST"})
     *
     * @param Request $request
     */
    public function removeAuthTokenAction(Request $request)
    {
        /** @var AuthTokenDocument */
        $authToken = $this->authTokenManager->getRepository()->findOneBy([
            'value' => $request->get('token')
        ]);

        $connectedUser = $this->getUser();
        if ($authToken
            && $connectedUser->getId()
            && $authToken->getUser()->getId() == $connectedUser->getId()) {

            $this->authTokenManager->getDocumentManager()->remove($authToken);
            $this->authTokenManager->getDocumentManager()->flush();

            return new JsonResponse([
                'success' => true
            ]);

        }

        return new JsonResponse([
            'success' => false
        ]);
    }
}