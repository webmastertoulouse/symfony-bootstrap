<?php

namespace App\Controller\Rest;

use App\Document\AuthTokenDocument;
use App\Document\UserDocument;
use App\Exception\ODMValidatorException;
use App\Form\Type\UserType;
use App\Manager\AuthTokenManager;
use App\Manager\UserManager;
use App\Request\DTO\UserDTO;
use App\Service\SerializerService;
use Doctrine\ODM\MongoDB\DocumentNotFoundException;
use FOS\RestBundle\View\View;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class UserController extends Controller
{
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var AuthTokenManager
     */
    private $authTokenManager;

    /**
     * @var UserDTO
     */
    private $userDTO;

    /**
     * @var SerializerService
     */
    private $serializerService;

    /**
     * @param LoggerInterface $logger
     * @param UserManager $userManager
     * @param AuthTokenManager $authTokenManager
     * @param UserDTO $userDTO
     */
    public function __construct(LoggerInterface $logger,
                                UserManager $userManager,
                                AuthTokenManager $authTokenManager,
                                UserDTO $userDTO,
                                SerializerService $serializerService)
    {
        $this->logger               = $logger;
        $this->userManager          = $userManager;
        $this->authTokenManager     = $authTokenManager;
        $this->userDTO              = $userDTO;
        $this->serializerService    = $serializerService;
    }

    /**
     * Get current connected user
     *
     * @Route("/users/token", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getUsersByTokenAction(Request $request)
    {
        $token      = $request->headers->get('X-Auth-Token');
        /** @var AuthTokenDocument $authToken */
        $authToken  = $this->authTokenManager->getRepository()->findOneBy([
            'value' => $token
        ]);

        try {
            if ($authToken->getUser()
                && $this->getUser()
                && $authToken->getUser()->getId() == $this->getUser()->getId()) {

                $this->userDTO->setDocument($authToken->getUser());
                return new JsonResponse([
                    'user'  => $this->serializerService->serialize('App\Request\DTO\UserDTO', $authToken->getUser())
                ]);
            }

        } catch (\Exception $e) {
            $this->logger->error('Error Info', [
                'class'     => __CLASS__,
                'function'  => __FUNCTION__,
                'error'     => $e->getMessage(),
                'file'      => $e->getFile(),
                'line'      => $e->getLine(),
            ]);
        }

        return new JsonResponse();
    }

    /**
     * Create a user
     *
     * @Route("/users", methods={"POST"}, name="create_user")
     * @param UserDTO $userDTO
     * @return View
     */
    public function postUsersAction(UserDTO $userDTO)
    {
        try {
            $this->logger->info('Create valid user action', [
                'class'     => __CLASS__,
                'method'    => __FUNCTION__,
                'user'      => $userDTO->toArray()
            ]);

            /** @var UserDocument $user */
            $user = $this->userManager->create($userDTO->fromDTO());
            $authToken = $this->authTokenManager->getAuthToken($user);

            $userContent        = $this->serializerService->serialize('App\Request\DTO\UserDTO', $user, 'json');
            $authTokenContent   = $this->serializerService->serialize('App\Request\DTO\AuthTokenDTO', $authToken, 'json');

            return View::create([
                'success'       => true,
                'auth_token'    => $authTokenContent,
                'user'          => $userContent
            ], Response::HTTP_OK);

        } catch (ODMValidatorException $e) {
            return View::create([
                'error'         => true,
                'message'       => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        } catch (\MongoDuplicateKeyException $e) {
            return View::create([
                'error'         => true,
                'message'       => 'LOGIN.ERROR.DUPLICATE_KEY'
            ], Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return View::create([
                'error'         => true,
                'message'       => $e->getMessage()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Confirm user with confirmKey
     * @Route("/users/confirm/{key}", methods={"GET"}, name="confirm_user", requirements={"key"=".+"})
     * @param Request $request
     * @return Response
     */
    public function getUserConfirmAction(Request $request)
    {
        /** @var UserDocument $user */
        $user = $this->userManager->getRepository()->findOneBy([
            'confirmKey' => $request->get('key')
        ]);

        if ($user) {
            $user->setConfirmKey(null);
            $user->setConfirmed(true);
            $this->userManager->getDocumentManager()->persist($user);
            $this->userManager->getDocumentManager()->flush();
        }

        $response = new Response(
            'Content',
            Response::HTTP_OK,
            array('content-type' => 'text/html')
        );

        return $this->render('user/account-confirmed.html.twig', [], $response);
    }

    /**
     * @Route("/users/{id}", methods={"PUT"})
     *
     * @param Request $request
     * @param UserDocument $user
     * @param UserDocument $originalUser
     *
     * @ParamConverter("user", class="App\Document\UserDocument", converter="form_converter")
     * @ParamConverter("originalUser", class="App\Document\UserDocument", converter="form_converter")
     *
     * @return mixed
     *
     * @throws ODMValidatorException
     */
    public function updateUserAction(Request $request, UserDocument $user, UserDocument $originalUser)
    {
        if (is_null($originalUser->getId())) {
            throw new DocumentNotFoundException();
        }

        $form = $this->createForm(UserType::class, $user);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $this->userManager->update($user, $originalUser);
            $response = new JsonResponse();
        } else {
            $response = new JsonResponse((array) $form->getData());
        }

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}