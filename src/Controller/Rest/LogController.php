<?php

namespace App\Controller\Rest;

use App\Manager\UserManager;
use App\Repository\LogRepository;
use App\Request\DTO\LogDTO;
use FOS\RestBundle\View\View;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LogController extends Controller
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var LogRepository
     */
    private $logRepository;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @param LoggerInterface $logger
     * @param LogRepository $logRepository
     * @param UserManager $userManager
     */
    public function __construct(LoggerInterface $logger, LogRepository $logRepository, UserManager $userManager)
    {
        $this->logger           = $logger;
        $this->logRepository    = $logRepository;
        $this->userManager      = $userManager;
    }

    /**
     * Create a user
     *
     * @Route("/logs", methods={"POST"})
     * @param LogDTO $logDTO
     * @return JsonResponse
     */
    public function postLogsAction(LogDTO $logDTO)
    {
        if ($logDTO) {
            $log = $logDTO->fromDTO($logDTO);

            if ($this->getUser() && $this->getUser()->getId()) {
                $user = $this->userManager->getRepository()->findOneBy([
                    'id' => $this->getUser()->getId()
                ]);

                $log->setUser($user);
            }

            $this->logRepository->getDocumentManager()->persist($log);
            $this->logRepository->getDocumentManager()->flush();

            return View::create([], Response::HTTP_CREATED);
        } else {
            return View::create([], Response::HTTP_BAD_REQUEST);
        }
    }
}