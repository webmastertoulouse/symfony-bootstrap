<?php

namespace App\Controller\Web;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param LoggerInterface $logger
     * @param UserManager $userManager
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger       = $logger;
    }

    /**
     * @Route("/", methods={"GET"})
     */
    public function index()
    {
        return $this->render('base.html.twig');
    }
}