<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\MongoDB\Connection;
use Doctrine\ODM\MongoDB\Configuration;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;
use App\Document\User;

$appDir = __DIR__ . '/..';

if ( ! file_exists($file = $appDir . '/vendor/autoload.php')) {
    throw new RuntimeException('Install dependencies to run this script.');
}

$loader = require_once $file;
$loader->add('App/Document', $appDir . '/src');

AnnotationRegistry::registerLoader([$loader, 'loadClass']);

$connection = new Connection();

$config = new Configuration();
$config->setProxyDir($appDir . '/var/cache/dev/doctrine/odm/mongodb/Proxies');
$config->setProxyNamespace('App\Proxie');
$config->setHydratorDir($appDir . '/var/cache/dev/doctrine/odm/mongodb/Hydrators');
$config->setHydratorNamespace('App\Hydrator');
$config->setDefaultDB('my_story');
$config->setMetadataDriverImpl(AnnotationDriver::create($appDir . '/src/Document'));
$config->addFilter('deletedAt_filter', 'App\Document\Filter\deletedAtFilter');

function createUser($connection, $config) {

    $dm = DocumentManager::create($connection, $config);

    $user = new User();
    $user->setUsername('Username');
    $user->setFirstname('Jean');
    $user->setGivenname('Espinasse');
    $user->setEmail('jbe.webmaster@gmail.com');

    $dm->persist($user);
    $dm->flush();
}

function removeUser($id, $connection, $config) {

    $dm = DocumentManager::create($connection, $config);

    /** @var User $user */
    $user = $dm->getRepository(User::class)->find($id);
    if ($user) {
        $user->setDeletedAt(new DateTime());
        $dm->flush();
    }
}

function listUser($connection, $config) {

    $dm = DocumentManager::create($connection, $config);
    $dm->getFilterCollection()->enable("deletedAt_filter");

    /** @var User[] $users */
    $users = $dm->getRepository(User::class)->findAll();

    foreach ($users AS $user) {
        echo "{$user->getId()}, {$user->getUsername()} {$user->getCreatedAt()->format('d/m/Y')}\n";
    }

    return $users;
}

/**
 * @param $id
 * @param $connection
 * @param $config
 * @return User
 */
function findUserByCriteria($id, $connection, $config) {

    $dm = DocumentManager::create($connection, $config);
    $dm->getFilterCollection()->enable("deletedAt_filter");

    return $dm->getRepository(User::class)->findOneBy(['_id' => $id]);
}

/**
 * @param $id
 * @param $connection
 * @param $config
 * @return User
 */
function findUser($id, $connection, $config) {

    $dm = DocumentManager::create($connection, $config);
    $dm->getFilterCollection()->enable("deletedAt_filter");

    return $dm->getRepository(User::class)->find($id);
}

// createUser($connection, $config);

$id = '5b326213925d603b1d77d6c1';

$users = listUser($connection, $config);
echo count($users)."\n";
removeUser($id, $connection, $config);
echo "\n";
$users = listUser($connection, $config);
echo count($users)."\n";
$user = findUser($id, $connection, $config);
if ($user) {
    echo $user->getId()."\n";
} else {
    echo "User not found\n";
}
// $user = findUserByCriteria($id, $connection, $config);
// echo count($users)."\n";
